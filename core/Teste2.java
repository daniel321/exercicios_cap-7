package core;

import model.Conta;
import model.ContaCorrente;
import model.ContaPoupanca;

public class Teste2 {
	public static void main(String[] args) {
		Conta conta = new Conta();
		Conta cPoupanca = new ContaPoupanca();
		Conta cCorrente = new ContaCorrente();
		
		conta.depositar(1000);
		cPoupanca.depositar(1000);
		cCorrente.depositar(1000);
		
		conta.atualizar(0.01);
		cPoupanca.atualizar(0.01);
		cCorrente.atualizar(0.01);
		
		System.out.println(conta.getSaldo());
		System.out.println(cPoupanca.getSaldo());
		System.out.println(cCorrente.getSaldo());
	}
	/*Rodou perfeitamente, mas os resultados n�o foram diferentes do 1�.
	  A mudan�a foi instanciar o objetos com a superclasse e herdar 
	  as caracter�sticas das subclasses.
	  Aparentemente, n�o h� utilidade nisto.*/
}
