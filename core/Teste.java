package core;

import model.*;

public class Teste {
	public static void main(String[] args) {
		Conta conta = new Conta();
		ContaPoupanca cPoupanca = new ContaPoupanca();
		ContaCorrente cCorrente = new ContaCorrente();
		
		conta.depositar(1000);
		cPoupanca.depositar(1000);
		cCorrente.depositar(1000);
		
		conta.atualizar(0.01);
		cPoupanca.atualizar(0.01);
		cCorrente.atualizar(0.01);
		
		System.out.println(conta.getSaldo());
		System.out.println(cPoupanca.getSaldo());
		System.out.println(cCorrente.getSaldo());
	}
	//Ao imprimir os saldos, o saldo da poupan�a ficou 1019.898, onde deveria sair 1020.0. Provalvelmente por conta da precis�o da convers�o do double.
}
